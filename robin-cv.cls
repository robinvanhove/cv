\ProvidesClass{robin-cv}
\NeedsTeXFormat{LaTeX2e}
\LoadClass{article}
\usepackage[a4paper, top=1.5cm, bottom=1.5cm, left=1cm, right=1cm]{geometry}
\RequirePackage{multicol}
\RequirePackage{hyperref}
\RequirePackage{xcolor}
\RequirePackage{xifthen}
\RequirePackage[relative]{textpos}
\RequirePackage{iflang}
\RequirePackage{titlesec}
\RequirePackage{fontawesome}
\RequirePackage{enumitem}

\input{glyphtounicode}
\pdfgentounicode=1

\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000


% Seperation between itemize items in whole document
\setitemize{itemsep=0em}

\def\firstname#1{\gdef\@firstname{#1}}
\def\lastname#1{\gdef\@lastname{#1}}
\def\title#1{\gdef\@title{#1}}
\def\email#1{\gdef\@email{#1}}
\def\address#1{\gdef\@address{#1}}
\def\website#1{\gdef\@website{#1}}
\def\phone#1{\gdef\@phone{#1}}
\def\pgp#1{\gdef\@pgp{#1}}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HEADER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\makeheader}{
\begin{multicols}{2}
{\bf \Huge\@firstname} \ {\bf \Huge\@lastname}
\vspace{0.2cm}


\ifdefined \@title {\Large \@title} \\ \fi
\vfill
~
\columnbreak
\begin{flushright}
\begin{tabular}{c l}
\ifdefined \@address \faMapMarker & \href{geo:51.0263143,4.4742301}{\@address} \\ \smallskip \fi
\ifdefined \@email \faEnvelope & \href{mailto:\@email}{\@email} \\ \fi
%\ifdefined \@pgp PGP: \@pgp \\ \fi
\ifdefined \@website \href{http://\@website}{\@website} \\ \fi
\ifdefined \@phone \faPhone & \href{tel:\@phone}{\@phone} \\ \fi
\end{tabular}
\end{flushright}
\end{multicols}
\noindent {\rule{\textwidth}{1.5px}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SECTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reducine spacing around section titles
\titlespacing*{\section}
{0ex}{1em}{0em}
\titlespacing*{\subsection}
{0ex}{0em}{0em}


\renewcommand{\thesection}{}
\def\@seccntformat#1{\csname #1ignore\expandafter\endcsname\csname the#1\endcsname\quad}
\let\sectionignore\@gobbletwo
\let\latex@numberline\numberline
\def\numberline#1{\if\relax#1\relax\else\latex@numberline{#1}\fi}
\setlength{\parindent}{0cm}

\newcommand{\cvsection}[1]{
    \section{#1} 
    \vspace{-0.65em}
    {\rule{\textwidth}{0.5px}}
    \vspace{-0.35em}

}

\renewcommand{\thesubsection}{}
\def\@seccntformat#1{\csname #1ignore\expandafter\endcsname\csname the#1\endcsname\quad}
\let\subsectionignore\@gobbletwo
\let\latex@numberline\numberline
\def\numberline#1{\if\relax#1\relax\else\latex@numberline{#1}\fi}
\setlength{\parindent}{0cm}

\newcommand{\cvsubsection}[1]{
    \subsection{#1} 
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\TPHorizModule}{1sp}
\setlength{\TPVertModule}{1sp}

\newlength{\cvmainwidth}
\newlength{\cvasidewidth}
\newlength{\cvspacing}
\newlength{\cvasidestart}
\setlength{\cvmainwidth}{0.65\textwidth}
\setlength{\cvspacing}{1cm}


\newenvironment{main}{%
  \begin{textblock}{\number\cvmainwidth}(0, 0)
}{
  \end{textblock}
}

\newenvironment{aside}{%
  \setlength{\cvasidewidth}{\textwidth - \cvmainwidth - \cvspacing}
  \setlength{\cvasidestart}{\cvmainwidth + \cvspacing}
  \begin{textblock}{\number\cvasidewidth}(\number\cvasidestart, 30000)
}{
  \end{textblock}
}

\setlength{\multicolsep}{0pt}

\newcommand{\cvitem}[5]{
\begin{multicols}{2}
{\bf \large#1}  \\
\mbox{#4} 
\vfill

\begin{flushright}
{\large #2}  \\
#3
\end{flushright}
\end{multicols}

\ifthenelse{\isempty{#5}}{\vspace{0.6cm}}{#5 \vspace{0.2cm}}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FOOTER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\makefooter}{
\vfill
\begin{center}
\color{lightgray}
\hfill
\small
\IfLanguageName{dutch}
{Laatste wijziging}
{Last change}

\hfill
\today
\end{center}
}
